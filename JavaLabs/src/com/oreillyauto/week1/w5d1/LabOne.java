package com.oreillyauto.week1.w5d1;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Week 5 Day 1 Lab
 * 
 * - Expected Output - 
 * Total Payroll: 60000.00
 * Manager Payroll: 40000.00
 * Giving A $500 raise to each employee...
 * Total Payroll: 61000.00
 * Manager Payroll: 40500.00
 * Age Check:
 * Name: Jeffery Brannon Age: 30
 * Name: Bob Evans Age: 30
 * Jeffery Had A Birthday!
 * Age Check:
 * Name: Jeffery Brannon Age: 31
 * Name: Bob Evans Age: 30
 * 
 */
public class LabOne {
	
	// global employee list goes here !!!!!!!!!
    
    private List<OReillyEmployee> employeeList = new ArrayList<OReillyEmployee>();
	
	public static final String DEFAULT_RAISE = "500.00";
	
    public LabOne() {
        OReillyEmployee employeeOne = new OReillyEmployee(1L, 30, "Jeffery Brannon", 
        		new BigDecimal("20000.00"), "Supervisor");
        OReillyEmployee employeeTwo = new OReillyEmployee(1L, 30, "Bob Evans", 
        		new BigDecimal("40000.00"), "Manager");
        employeeList.add(employeeOne);
        employeeList.add(employeeTwo);
		
		System.out.println("Total Payroll: " + calculateCompanyPayroll());
		System.out.println("Manager Payroll: " + calculateCompanyPayroll("Manager"));
		
		// Using a public static final default value, give raises 
		System.out.println("Give the default raise to each employee...");
		giveAnnualRaiseForCompany(new BigDecimal(DEFAULT_RAISE));
		
		System.out.println("Total Payroll: " + Payroll.calculatePayroll(employeeList));
		System.out.println("Manager Payroll: " + Payroll.calculatePayroll(employeeList, "Manager"));
		
		System.out.println("Age Check:");
		
		printEmployeeNamesAndAges(employeeList);
		
		//payroll.happyBirthday(employeeOne);
		System.out.println("Jeffery Had A Birthday!");
		
		System.out.println("Age Check:");
		
		printEmployeeNamesAndAges(employeeList);
    }

	private void printEmployeeNamesAndAges(List<OReillyEmployee> employeeList) {
		for (OReillyEmployee oReillyEmployee : employeeList) {
		    String name = oReillyEmployee.getName();
		    Integer age = oReillyEmployee.getAge();
            System.out.println("Name: " + name + " Age: " + age);
        
        }
	}

	private void giveAnnualRaiseForCompany(BigDecimal amount) {
		// call Payroll class
	    Payroll.giveRaises(employeeList, amount);
		
	}

	// Total company payroll
	private BigDecimal calculateCompanyPayroll() {
		// call Payroll class and return the company payroll
	    return Payroll.calculatePayroll(employeeList);
	}
	
	// Total company payroll by title
	private BigDecimal calculateCompanyPayroll(String title) {
		// call Payroll class
	    return Payroll.calculatePayroll(employeeList, title);
	}
	
    // Main method goes here
	// ???
	public static void main(String[] args) {
	    new LabOne();
	}
	// List Help
	// List<ObjectType> myList = new ArrayList<ObjectType>();
	// * Add to a list
	// ObjectType t = new ObjectType();
	// myList.add(t);
	
}

