package com.oreillyauto.week1.w5d1;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class Payroll {

    public static BigDecimal calculatePayroll(List<OReillyEmployee> employeeList) {
        
        BigDecimal total = new BigDecimal("0.00");
        
        for (OReillyEmployee oReillyEmployee : employeeList) {
            total = total.add(oReillyEmployee.getSalary());
        }
        
        return total;
    }

    public static BigDecimal calculatePayroll(List<OReillyEmployee> employeeList, String title) {
        // TODO Auto-generated method stub
        BigDecimal total = new BigDecimal("0.00");
        
        for (OReillyEmployee oReillyEmployee : employeeList) {
            String empTitle = oReillyEmployee.getTitle();
            if (empTitle == title) {
                total = total.add(oReillyEmployee.getSalary());
            }
        }
        return total;
    }

    public static void giveRaises(List<OReillyEmployee> employeeList, BigDecimal amount) {
        // TODO Auto-generated method stub
        for (OReillyEmployee oReillyEmployee : employeeList) {
            BigDecimal salary = oReillyEmployee.getSalary();
            salary = salary.add(amount);
            oReillyEmployee.setSalary(salary);
        }
    }

    

    
    
}
