package com.oreillyauto.week1.w5d1;

import java.math.BigDecimal;

public class OReillyEmployee {
	private Long id;
	private Integer age;
	private String name;
	private BigDecimal salary;
	private String title;
	
	public OReillyEmployee(){
	    super();
	}
	
	public OReillyEmployee(Long id, Integer age, String name, BigDecimal salary, String title) {
        super();
        this.id = id;
        this.age = age;
        this.name = name;
        this.salary = salary;
        this.title = title;
    }

    public long getId() {
	    return id;
	}
	
	public int getAge() {
	    return age;
	}
	
	public String getName() {
	    return name;
	}
	
	public BigDecimal getSalary() {
	    return salary;
	}
	
	public String getTitle() {
	    return title;
	}
	
	public void setId(int id) {
	    this.id = id;
	}
	
	public void setAge(int age) {
	    this.age = age;
	}
	
	public void setName(String name) {
	    this.name = name;
	}
	
	public void setSalary(BigDecimal salary) {
	    this.salary = salary;
	}
	
	public void setTitle(String title) {
	    this.title = title;
	}
}
