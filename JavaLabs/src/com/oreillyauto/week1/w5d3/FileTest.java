package com.oreillyauto.week1.w5d3;

import java.io.File;

public class FileTest {
    public FileTest() {
        // The default classpath is the actual project. It doesn't matter in what package you're
        // running your code. Since we have numbers.txt at the root of the project, we can access it
        // without any other reference. 
        File file = new File("numbers.txt");
        System.out.println("file.exists (root location): " + (file.exists()));
        // Output: file.exists (root location): true
        
//        File file2 = new File(getClass().getResource("/com/oreillyauto/w5d3/numbers.txt").getFile());   
//        System.out.println("file2.exists (package): " + (file2.exists()));
        // This does not return true because my file is at the root.
           }
    
    private void testClassPathResource() {
        File file = new File(getClass().getResource("").getFile());
        System.out.println("file.exists: " + (file.exists()));
    }
    
    public static void main(String[] args) {
        new FileTest();
    }
}
