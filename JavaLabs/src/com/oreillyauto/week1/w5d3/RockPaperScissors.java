package com.oreillyauto.week1.w5d3;

import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
    
    public enum RPS{
        ROCK, PAPER, SCISSORS;
    }
    
    public RockPaperScissors() {
        RPS pcMove = RPS.SCISSORS;
        Random rand = new Random();
        int randInt = rand.nextInt(3);
        if(randInt == 0) {
            pcMove = RPS.ROCK;
        }
        else if(randInt == 1) {
            pcMove = RPS.PAPER;
        }
        else if(randInt == 0) {
            pcMove = RPS.SCISSORS;
        }
        Scanner scanner = new Scanner(System.in);
        System.out.println("Type (1) for Rock, (2) for Paper, (3) for Scissors: ");
        String move = scanner.nextLine();
        if("1".equals(move)|| "2".equals(move) || "3".equals(move)) {
            System.out.println(move);
            //scanner.close();
            
        } else {
            System.out.println("Fail");
        }
        
        
        switch(pcMove) {
            case ROCK:
                System.out.println(pcMove.name());
                break;
            case PAPER:
                System.out.println(pcMove.name());
                break;
            case SCISSORS:
                System.out.println(pcMove.name());
                break;
        }
    }
    
    public static void main(String[] args) {
        new RockPaperScissors();
    }
    
    
}